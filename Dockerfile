FROM python:3.10.4-slim-bullseye

WORKDIR /usr/src/rtxstock

RUN python -m venv /usr/venv

ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONUNBUFFERED=1 \
    VIRTUAL_ENV="/usr/venv/" \
    POETRY_NO_INTERACTION=true \
    POETRY_HOME="/opt/poetry"

ENV PATH="$POETRY_HOME/bin:$VIRTUAL_ENV/bin:$PATH"

RUN apt update && apt upgrade -y
RUN apt install -y libpq-dev gcc
RUN apt clean
RUN pip install --no-cache-dir --upgrade pip
RUN pip install --no-cache-dir poetry
COPY ./poetry.lock /pyproject.toml ./
RUN poetry install && yes | poetry cache clear . --all

COPY . .

CMD python manage.py runserver 0.0.0.0:8000
