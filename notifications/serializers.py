from rest_framework import serializers

from notifications.models import NotificationRequest


class NotificationRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = NotificationRequest
        fields = ("id", "email", "sent", "specific_models", "created")
        read_only_fields = ("id", "sent", "created")
