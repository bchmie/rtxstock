import factory
from django.utils import timezone
from factory.django import DjangoModelFactory

from notifications.models import NotificationRequest


class NotificationRequestFactory(DjangoModelFactory):
    class Meta:
        model = NotificationRequest

    email = factory.Faker("email")
    sent = False
    created = factory.LazyFunction(timezone.now)

    @factory.post_generation
    def specific_models(self, create, extracted, **kwargs):
        if not create:
            return
        if extracted:
            for specific_model in extracted:
                self.specific_models.add(specific_model)
