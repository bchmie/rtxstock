from django.db import models
from django.db.models import Exists, Q

from rtxstock.models import Product


class NotificationRequestManager(models.Manager):
    def pending_notifications(self):
        """Returns notifications that are waiting to be send."""
        cards_available = Product.objects.filter(is_available=True)
        return self.filter(
            Exists(cards_available),
            Q(specific_models__isnull=True) | Q(specific_models__in=cards_available),
            sent=False,
        )


class NotificationRequest(models.Model):
    email = models.EmailField()
    sent = models.BooleanField(default=False)
    specific_models = models.ManyToManyField(Product, blank=True)
    created = models.DateTimeField(auto_now_add=True)

    objects = NotificationRequestManager()

    def to_email(self):
        """Returns data tuple used in django.core.mail.mass_send_email."""
        subject = "RTX3080 in stock notification"
        message = """
        One of cards is now in stock, checkout our page.

        Regards,
        Rtxstock
        """
        return subject, message, None, [self.email]
