import smtplib

from celery import shared_task
from django.core.mail import send_mass_mail

from notifications.models import NotificationRequest


@shared_task
def send_notifications():
    pending_notifications = NotificationRequest.objects.pending_notifications()
    try:
        send_mass_mail(
            (notification.to_email() for notification in pending_notifications),
            fail_silently=False,
        )
    except smtplib.SMTPException:
        pass
    else:
        for notification in pending_notifications:
            notification.sent = True
        NotificationRequest.objects.bulk_update(pending_notifications, ["sent"])
