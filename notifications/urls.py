from django.urls import path

from notifications.views import (
    NotificationRequestDetailApi,
    NotificationRequestListCreateApi,
)

urlpatterns = [
    path(
        "notifications/",
        NotificationRequestListCreateApi.as_view(),
        name="notifications-list-create",
    ),
    path(
        "notifications/<int:pk>/",
        NotificationRequestDetailApi.as_view(),
        name="notifications-detail",
    ),
]
