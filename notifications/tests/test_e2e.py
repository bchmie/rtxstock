import pytest
from rest_framework.reverse import reverse

from notifications.factories import NotificationRequestFactory
from notifications.models import NotificationRequest
from notifications.serializers import NotificationRequestSerializer
from rtxstock.factories import ProductFactory


@pytest.mark.django_db
class TestE2E:
    def test_list_notifications(self, client):
        notifications = NotificationRequestFactory.create_batch(3)
        response = client.get(reverse("notifications:notifications-list-create"))
        for notification in NotificationRequestSerializer(
            notifications, many=True
        ).data:
            assert notification in response.data

    def test_get_notifications(self, client):
        notification = NotificationRequestFactory()
        response = client.get(
            reverse("notifications:notifications-detail", args=(notification.id,))
        )
        assert response.data == NotificationRequestSerializer(notification).data

    def test_get_notifications_with_relations(self, client):
        products = ProductFactory.create_batch(3)
        notification = NotificationRequestFactory(specific_models=products)
        response = client.get(
            reverse("notifications:notifications-detail", args=(notification.id,))
        )
        assert response.data == NotificationRequestSerializer(notification).data

    def test_post_notifications_no_relations(self, client):
        response = client.post(
            reverse("notifications:notifications-list-create"),
            data={"email": "abc@example.com", "specific_models": []},
        )
        notification = NotificationRequest.objects.first()
        assert response.data == NotificationRequestSerializer(notification).data
        assert len(response.data["specific_models"]) == 0

    def test_post_notifications_with_specific_models(self, client):
        products = ProductFactory.create_batch(3)

        response = client.post(
            reverse("notifications:notifications-list-create"),
            data={
                "email": "abc@example.com",
                "specific_models": [product.model for product in products],
            },
        )
        notification = NotificationRequest.objects.first()
        assert response.data == NotificationRequestSerializer(notification).data
        assert len(response.data["specific_models"]) == 3
