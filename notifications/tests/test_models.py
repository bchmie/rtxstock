import pytest

from notifications.factories import NotificationRequestFactory
from notifications.models import NotificationRequest
from rtxstock.factories import ProductFactory


@pytest.mark.django_db
class TestNotificationRequest:
    def test_pending_notifications_when_no_cards_are_available(self):
        ProductFactory(is_available=False)
        NotificationRequestFactory()
        notifications = NotificationRequest.objects.pending_notifications()
        assert len(notifications) == 0

    def test_pending_notifications_return_cards_are_available(self):
        ProductFactory(is_available=True)
        NotificationRequestFactory()
        notifications = NotificationRequest.objects.pending_notifications()
        assert len(notifications) == 1

    def test_pending_notifications_does_not_return_already_sent_notifications(self):
        ProductFactory(is_available=True)
        NotificationRequestFactory(sent=True)
        notifications = NotificationRequest.objects.pending_notifications()
        assert len(notifications) == 0

    def test_pending_notifications_with_specific_models_available(self):
        product = ProductFactory(is_available=True)
        NotificationRequestFactory(specific_models=[product])
        notifications = NotificationRequest.objects.pending_notifications()
        assert len(notifications) == 1

    def test_pending_notifications_with_specific_models_not_available(self):
        ProductFactory(is_available=True)
        specific_model = ProductFactory(is_available=False)
        NotificationRequestFactory(specific_models=[specific_model])
        notifications = NotificationRequest.objects.pending_notifications()
        assert len(notifications) == 0


class TestNotificationRequestToEmail:
    def test_to_email(self):
        notification = NotificationRequestFactory.build()
        subject, message, from_email, recipient_list = notification.to_email()
        assert subject == "RTX3080 in stock notification"
        assert "One of cards is now in stock, checkout our page." in message
        assert from_email is None
        assert recipient_list == [notification.email]
