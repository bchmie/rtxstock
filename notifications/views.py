from rest_framework import status
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView

from notifications.models import NotificationRequest
from notifications.serializers import NotificationRequestSerializer


class NotificationRequestListCreateApi(APIView):
    def get(self, request):
        notifications = NotificationRequest.objects.all()
        serializer = NotificationRequestSerializer(notifications, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = NotificationRequestSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class NotificationRequestDetailApi(APIView):
    def get(self, request, pk):
        notification = get_object_or_404(NotificationRequest, pk=pk)
        serializer = NotificationRequestSerializer(notification)
        return Response(serializer.data)
