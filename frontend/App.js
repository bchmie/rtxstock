import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import TopBar from "./TopBar";
import ProductList from "./ProductList";
import {
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  makeStyles,
  ThemeProvider,
  Toolbar,
} from "@material-ui/core";
import { createTheme } from "@material-ui/core/styles";
import NotificationsActiveIcon from "@material-ui/icons/NotificationsActive";
import TimelineIcon from "@material-ui/icons/Timeline";
import TocIcon from "@material-ui/icons/Toc";
import Statistics from "./Statistics";
import Notifications from "./Notifications";

const drawerWidth = 240;

const theme = createTheme();

const useStyles = makeStyles({
  wrapper: {
    display: "flex",
    flex: "1 1 auto",
    overflow: "hidden",
  },
  drawerPaper: {
    width: drawerWidth,
  },
  drawerContainer: {
    overflow: "auto",
  },
});

const App = () => {
  const [products, setProducts] = useState([]);
  const classes = useStyles();

  useEffect(() => {
    const fetchProducts = async () => {
      const res = await fetch(process.env.API_ENDPOINT + "/api/v1/products/");
      const data = await res.json();
      setProducts(data);
    };
    fetchProducts();
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <Router>
        <TopBar />
        <Drawer
          className={classes.drawer}
          variant="permanent"
          classes={{
            paper: classes.drawerPaper,
          }}
        >
          <Toolbar />
          <div className={classes.drawerContainer}>
            <List>
              <ListItem button component={Link} to={"/"}>
                <ListItemIcon>
                  <TocIcon />
                </ListItemIcon>
                <ListItemText primary="Table" />
              </ListItem>
              <ListItem button component={Link} to={"/statistics/"}>
                <ListItemIcon>
                  <TimelineIcon />
                </ListItemIcon>
                <ListItemText primary="Statistics" />
              </ListItem>
              <ListItem button component={Link} to={"/notifications/"}>
                <ListItemIcon>
                  <NotificationsActiveIcon />
                </ListItemIcon>
                <ListItemText primary="Notifications" />
              </ListItem>
            </List>
          </div>
        </Drawer>
        <div className={classes.wrapper}>
          <Switch>
            <Route path="/" exact>
              <ProductList products={products} />
            </Route>
            <Route path="/statistics" exact>
              <Statistics />
            </Route>
            <Route path="/notifications" exact>
              <Notifications products={products} />
            </Route>
          </Switch>
        </div>
      </Router>
    </ThemeProvider>
  );
};

export default App;
