import React, { useEffect, useState } from "react";
import { Container, Grid, Paper, Typography } from "@material-ui/core";
import AvailableCardsChart from "./charts/AvailableCardsChart";
import DurationSelect from "./charts/DurationSelect";
import { makeStyles } from "@material-ui/core/styles";
import { filterItemsOutsideDuration } from "./charts/utils";
import StatisticsChart from "./charts/StatisticsChart";

const useStyles = makeStyles({
  mainContent: {
    paddingTop: 96,
    paddingBottom: 24,
  },
  paper: {
    padding: 6,
    margin: 10,
  },
  chartContainer: {
    height: 300,
    width: "90%",
  },
});

const Statistics = () => {
  const [duration, setDuration] = useState(31);
  const [statistics, setStatistics] = useState([]);
  const classes = useStyles();

  const processData = (data) => {
    data.forEach((item) => {
      item.date = new Date(item.date).valueOf();
      item.price = parseFloat(item.price);
    });
  };

  useEffect(() => {
    const fetchStatistics = async () => {
      const res = await fetch(process.env.API_ENDPOINT + "/api/v1/statistics/");
      const data = await res.json();
      processData(data);
      setStatistics(data);
    };
    fetchStatistics();
  }, []);

  return (
    <Container maxWidth="lg" className={classes.mainContent}>
      <Paper className={classes.paper}>
        <Grid container justifyContent="space-between" alignItems="center">
          <Grid item>
            <Typography variant="h5" gutterBottom component="div">
              Statistics for RTX3080 cards availability
            </Typography>
          </Grid>
          <Grid item>
            <DurationSelect duration={duration} setDuration={setDuration} />
          </Grid>
        </Grid>
      </Paper>
      <Paper className={classes.paper}>
        <Grid container justifyContent="center" alignItems="center">
          <Grid item>
            <Typography variant="h6" gutterBottom component="div">
              Number of cards available
            </Typography>
          </Grid>
          <Grid item className={classes.chartContainer}>
            <AvailableCardsChart
              statistics={filterItemsOutsideDuration(statistics, duration)}
              duration={duration}
            />
          </Grid>
        </Grid>
      </Paper>
      <Paper className={classes.paper}>
        <Grid container justifyContent="center" alignItems="center">
          <Grid item>
            <Typography variant="h6" gutterBottom component="div">
              Mean price of a RTX3080 GPU
            </Typography>
          </Grid>
          <Grid item className={classes.chartContainer}>
            <StatisticsChart
              statistics={filterItemsOutsideDuration(statistics, duration)}
              duration={duration}
              dataKey="mean_price"
            />
          </Grid>
        </Grid>
      </Paper>
      <Paper className={classes.paper}>
        <Grid container justifyContent="center" alignItems="center">
          <Grid item>
            <Typography variant="h6" gutterBottom component="div">
              Median price of a RTX3080 GPU
            </Typography>
          </Grid>
          <Grid item className={classes.chartContainer}>
            <StatisticsChart
              statistics={filterItemsOutsideDuration(statistics, duration)}
              duration={duration}
              dataKey="median_price"
            />
          </Grid>
        </Grid>
      </Paper>
    </Container>
  );
};

export default Statistics;
