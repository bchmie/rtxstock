export const tickFormatter = (date) => {
  let result = new Date(date).toISOString().split("T")[0];
  return result;
};

export const calculateDomain = (duration) => {
  let stop = new Date();
  let temp = new Date();
  let start = temp.setDate(temp.getDate() - duration);
  return [start.valueOf(), stop.valueOf()];
};

export const filterItemsOutsideDuration = (items, duration) => {
  let d = new Date();
  d.setDate(d.getDate() - duration);
  return items.filter((item) => item.date >= d.valueOf());
};
