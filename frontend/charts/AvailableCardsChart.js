import React from "react";
import {
  Bar,
  BarChart,
  CartesianGrid,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { calculateDomain, tickFormatter } from "./utils";
import AvailableCardsChartTooltip from "./AvailableCardsChartTooltip";

const AvailableCardsChart = ({ statistics, duration }) => {
  return (
    <ResponsiveContainer width="100%" height="100%">
      <BarChart
        width={500}
        height={300}
        data={statistics}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis
          type="number"
          scale="time"
          dataKey="date"
          name="Date"
          domain={calculateDomain(duration)}
          tickFormatter={tickFormatter}
          allowDataOverflow={true}
        />
        <YAxis />
        <Tooltip
          content={<AvailableCardsChartTooltip />}
          cursor={{ strokeDasharray: "3 3" }}
        />
        <Bar dataKey="cards_available" fill="#3f51b5" />
      </BarChart>
    </ResponsiveContainer>
  );
};

export default AvailableCardsChart;
