import React from "react";
import {
  LineChart,
  CartesianGrid,
  Line,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { calculateDomain, tickFormatter } from "./utils";
import StatisticsChartTooltip from "./StatisticsChartTooltip";

const StatisticsChart = ({ statistics, duration, dataKey }) => {
  return (
    <ResponsiveContainer width="100%" height="100%">
      <LineChart
        width={500}
        height={300}
        data={statistics}
        margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis
          type="number"
          scale="time"
          dataKey="date"
          name="Date"
          domain={calculateDomain(duration)}
          tickFormatter={tickFormatter}
          allowDataOverflow={true}
        />
        <YAxis />
        <Tooltip
          content={<StatisticsChartTooltip isMean={dataKey === "mean_price"} />}
          cursor={{ strokeDasharray: "3 3" }}
        />
        <Line type="monotone" dataKey={dataKey} stroke="#3f51b5" />
      </LineChart>
    </ResponsiveContainer>
  );
};

export default StatisticsChart;
