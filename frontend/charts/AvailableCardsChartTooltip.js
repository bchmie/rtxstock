import React from "react";
import { Paper } from "@material-ui/core";
import { tickFormatter } from "./utils";

const AvailableCardsChartTooltip = ({ active, payload }) => {
  if (active && payload && payload.length) {
    return (
      <Paper style={{ padding: 6 }}>
        <div className="custom-tooltip">
          <p>{`Timestamp: ${tickFormatter(payload[0].payload.date)}`}</p>
          <p>{`Cards available: ${payload[0].payload.cards_available}`}</p>
        </div>
      </Paper>
    );
  } else {
    return <div></div>;
  }
};

export default AvailableCardsChartTooltip;
