import React from "react";
import { FormControl, InputLabel, MenuItem, Select } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  chartContainer: {
    height: 300,
    width: "90%",
  },
  formControl: {
    minWidth: 120,
    padding: 8,
  },
});

const durationMapping = [
  { value: 7, name: "week" },
  { value: 31, name: "month" },
  { value: 93, name: "quarter" },
  { value: 183, name: "six months" },
];

const DurationSelect = ({ duration, setDuration }) => {
  const classes = useStyles();

  return (
    <FormControl className={classes.formControl}>
      <InputLabel id="duration-select-label">Duration</InputLabel>
      <Select
        labelId="duration-select-label"
        id="duration-select"
        value={duration}
        onChange={(e) => setDuration(e.target.value)}
      >
        {durationMapping.map((item, index) => (
          <MenuItem key={index} value={item["value"]}>
            {item["name"]}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default DurationSelect;
