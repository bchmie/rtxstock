import React from "react";
import { Paper } from "@material-ui/core";
import { tickFormatter } from "./utils";

const PriceChartTooltip = ({ active, payload }) => {
  if (active && payload && payload.length) {
    return (
      <Paper style={{ padding: 6 }}>
        <div className="custom-tooltip">
          <p>{`Timestamp: ${tickFormatter(payload[0].value)}`}</p>
          <p>{`Price: ${payload[1].value} zł`}</p>
        </div>
      </Paper>
    );
  } else {
    return <div></div>;
  }
};

export default PriceChartTooltip;
