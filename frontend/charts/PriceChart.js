import React, { useEffect, useState } from "react";
import {
  CartesianGrid,
  Legend,
  ResponsiveContainer,
  Scatter,
  ScatterChart,
  Tooltip,
  XAxis,
  YAxis,
  ZAxis,
} from "recharts";
import { Box, Grid, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { calculateDomain, tickFormatter } from "./utils";
import DurationSelect from "./DurationSelect";
import PriceChartTooltip from "./PriceChartTooltip";

const useStyles = makeStyles({
  chartContainer: {
    height: 300,
    width: "90%",
  },
  formControl: {
    minWidth: 120,
    padding: 8,
  },
});

const PriceChart = ({ model }) => {
  const [data, setData] = useState({});
  const [duration, setDuration] = useState(31);
  const classes = useStyles();

  const sortData = (data) => {
    let handler = {
      set: function (obj, property, value) {
        if (!Object.prototype.hasOwnProperty.call(obj, property)) {
          obj[property] = [];
        }
        obj[property].push(value);
        return true;
      },
    };
    let sortedData = new Proxy({}, handler);

    data.forEach((item) => {
      item.timestamp = new Date(item.timestamp).valueOf();
      item.price = parseFloat(item.price);
      sortedData[item.shop.name] = item;
    });
    return sortedData;
  };

  useEffect(() => {
    const fetchOffers = async () => {
      const url =
        process.env.API_ENDPOINT +
        "/api/v1/products/" +
        model +
        "/price-history/" +
        duration.toString() +
        "/";
      const res = await fetch(url);
      const data = await res.json();
      const sortedData = sortData(data);
      setData(sortedData);
    };
    fetchOffers();
  }, [duration]);

  const chartStyle = [
    { fill: "#3f51b5", shape: "cross" },
    { fill: "#f44336", shape: "diamond" },
    { fill: "#002984", shape: "circle" },
    { fill: "#ba000d", shape: "square" },
  ];

  return (
    <Box margin={1}>
      <Grid container justifyContent="space-between" alignItems="center">
        <Grid item>
          <Typography variant="h6" gutterBottom component="div">
            Price history
          </Typography>
        </Grid>
        <Grid item>
          <DurationSelect duration={duration} setDuration={setDuration} />
        </Grid>
      </Grid>

      <Grid container justifyContent="center" alignItems="center">
        <Grid item className={classes.chartContainer}>
          <ResponsiveContainer width="100%" height="100%">
            <ScatterChart
              width={500}
              height={400}
              margin={{
                top: 20,
                right: 20,
                bottom: 20,
                left: 20,
              }}
            >
              <CartesianGrid />
              <XAxis
                type="number"
                scale="time"
                dataKey="timestamp"
                name="Date"
                domain={calculateDomain(duration)}
                tickFormatter={tickFormatter}
              />
              <YAxis type="number" dataKey="price" name="price" unit="zł" />
              <ZAxis type="number" range={[100]} />
              <Tooltip
                content={<PriceChartTooltip />}
                cursor={{ strokeDasharray: "3 3" }}
              />
              <Legend />
              {Object.values(data).map((dataSet, index) => (
                <Scatter
                  key={index}
                  name={dataSet[0]["shop"]["name"]}
                  data={dataSet}
                  fill={chartStyle[index]["fill"]}
                  line
                  shape={chartStyle[index]["shape"]}
                />
              ))}
            </ScatterChart>
          </ResponsiveContainer>
        </Grid>
      </Grid>
    </Box>
  );
};

export default PriceChart;
