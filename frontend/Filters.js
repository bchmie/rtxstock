import React from "react";
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Checkbox,
  FormControl,
  FormControlLabel,
  Grid,
  IconButton,
  Slider,
  TextField,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import DeleteIcon from "@material-ui/icons/Delete";
import Typography from "@material-ui/core/Typography";

const useStyles = makeStyles({
  filtersPanel: {
    marginTop: 15,
    marginBottom: 15,
  },
  grid: {
    width: "100%",
  },
  formControl: {
    margin: 10,
    minWidth: 100,
    maxWidth: 180,
  },
  slider: {
    width: 200,
  },
});

const Filters = ({
  name,
  setName,
  manufacturer,
  setManufacturer,
  available,
  setAvailable,
  priceRange,
  setPriceRange,
  clearFilters,
}) => {
  const classes = useStyles();

  return (
    <Accordion className={classes.filtersPanel}>
      <AccordionSummary
        expandIcon={<ExpandMoreIcon />}
        aria-controls="panel1a-content"
        id="panel1a-header"
        data-cy="filters"
      >
        <Typography>Filters</Typography>
      </AccordionSummary>
      <AccordionDetails>
        <Grid
          container
          justifyContent="space-around"
          alignItems="center"
          className={classes.grid}
        >
          <Grid item>
            <FormControl>
              <TextField
                value={name}
                label="Name"
                onChange={(e) => setName(e.target.value)}
                className={classes.formControl}
                data-cy="filters-name"
              />
            </FormControl>
          </Grid>
          <Grid item>
            <FormControl>
              <TextField
                value={manufacturer}
                label="Manufacturer"
                onChange={(e) => setManufacturer(e.target.value)}
                className={classes.formControl}
                data-cy="filters-manufacturer"
              />
            </FormControl>
          </Grid>
          <Grid item>
            <FormControlLabel
              control={
                <Checkbox
                  checked={available}
                  onChange={() => setAvailable(!available)}
                  name="available"
                />
              }
              label="Available"
              className={classes.formControl}
              data-cy="filters-availability"
            />
          </Grid>
          <Grid item className={classes.slider}>
            <Typography gutterBottom>Price range</Typography>
            <Slider
              min={0}
              max={15000}
              value={priceRange}
              onChange={(e, newVal) => setPriceRange(newVal)}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              getAriaValueText={(value) => `${value} zł`}
              data-cy="filters-price"
            />
          </Grid>
          <Grid item>
            <IconButton
              onClick={clearFilters}
              aria-label="delete"
              data-cy="filters-reset"
            >
              <DeleteIcon />
            </IconButton>
          </Grid>
        </Grid>
      </AccordionDetails>
    </Accordion>
  );
};

export default Filters;
