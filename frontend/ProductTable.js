import React, { useState } from "react";
import {
  Collapse,
  IconButton,
  Link,
  makeStyles,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  TableSortLabel,
  Typography,
} from "@material-ui/core";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import PriceChart from "./charts/PriceChart";

const useStyles = makeStyles({
  table: {
    minWidth: 950,
  },
  offerCell: {
    maxWidth: 130,
  },
  availableOfferLink: {
    marginRight: 4,
  },
  unavailableOfferLink: {
    marginRight: 4,
    color: "#ba000d",
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
});

const headCells = [
  { id: "name", main: true, label: "Model" },
  { id: "manufacturer", main: false, label: "Manufacturer" },
  { id: "is_available", main: false, label: "Availability" },
  { id: "price", main: false, label: "Price" },
  { id: "last_seen", main: false, label: "Last seen" },
  { id: "offers", main: false, label: "Offers" },
];

const ProductTableHead = ({ classes, order, orderBy, onRequestSort }) => {
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead data-cy="product-table-header">
      <TableRow>
        <TableCell />
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.main ? "left" : "right"}
            label={headCell.label}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
              data-cy={"sort-" + headCell.id}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

const formatTimestampToDate = (date) => {
  if (!date) {
    return "";
  } else {
    let temp = new Date(date);
    return temp.toISOString().split("T")[0];
  }
};

const Row = ({ row, classes }) => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <TableRow
        hover
        role="checkbox"
        tabIndex={-1}
        data-cy={"product-row-" + row.model}
      >
        <TableCell>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => setOpen(!open)}
          >
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        <TableCell component="th" scope="row">
          {row.name}
        </TableCell>
        <TableCell align="right">{row.manufacturer.name || ""}</TableCell>
        <TableCell align="right">
          {row.is_available ? <CheckIcon /> : <CloseIcon />}
        </TableCell>
        <TableCell align="right">{row.price} zł</TableCell>
        <TableCell align="right">
          <Typography variant="caption">
            {formatTimestampToDate(row.last_seen)}
          </Typography>
        </TableCell>
        <TableCell align="right" className={classes.offerCell}>
          {row.offers.map((offer) => (
            <Typography variant="caption" key={offer.id}>
              <Link
                href={offer.link}
                className={
                  offer.is_available
                    ? classes.availableOfferLink
                    : classes.unavailableOfferLink
                }
                target="_blank"
                rel="noopener"
              >
                {offer.shop.name}
              </Link>
            </Typography>
          ))}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <PriceChart model={row.model} />
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  );
};

const descendingComparator = (a, b, orderBy) => {
  if (orderBy === "name") {
    if (a[orderBy].toLowerCase() < b[orderBy].toLowerCase()) {
      return 1;
    } else {
      return -1;
    }
  }
  if (orderBy === "manufacturer") {
    if (a[orderBy].name.toLowerCase() < b[orderBy].name.toLowerCase()) {
      return 1;
    } else {
      return -1;
    }
  }
  if (a[orderBy] === null) {
    return 1;
  }
  if (b[orderBy] === null) {
    return -1;
  }
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
};

const getComparator = (order, orderBy) => {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
};

const stableSort = (array, comparator) => {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
};

const ProductTable = ({ rows }) => {
  const [order, setOrder] = useState("asc");
  const [orderBy, setOrderBy] = useState("model");
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const classes = useStyles();

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  return (
    <Paper>
      <TableContainer>
        <Table className={classes.table} data-cy="product-table">
          <ProductTableHead
            order={order}
            orderBy={orderBy}
            classes={classes}
            onRequestSort={handleRequestSort}
          />
          <TableBody>
            {stableSort(rows, getComparator(order, orderBy))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => (
                <Row key={row.model} row={row} classes={classes} />
              ))}
            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
  );
};

export default ProductTable;
