import React, { useState } from "react";
import {
  Button,
  Container,
  FormControl,
  Grid,
  Paper,
  Snackbar,
  TextField,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { Alert, Autocomplete } from "@material-ui/lab";
import getCookie from "./utils/csrftoken";

const useStyles = makeStyles({
  mainContent: {
    paddingTop: 96,
    paddingBottom: 24,
  },
  paper: {
    padding: 10,
    margin: 10,
  },
  grid: {
    width: "100%",
  },
  formControl: {
    margin: 10,
    minWidth: 100,
    maxWidth: 180,
  },
  modelChoice: {
    margin: 10,
    minWidth: 320,
    maxWidth: 800,
  },
});

const Notifications = ({ products }) => {
  const [email, setEmail] = useState();
  const [modelChoice, setModelChoice] = useState([]);
  const [openSuccess, setOpenSuccess] = useState(false);
  const [openError, setOpenError] = useState(false);
  const classes = useStyles();

  const createNotificationRequest = async () => {
    let csrftoken = getCookie("csrftoken");

    const body = {
      email: email,
      model_choices: modelChoice.map((model) => model.name),
    };
    await fetch(process.env.API_ENDPOINT + "/api/v1/notifications/", {
      credentials: "include",
      method: "POST",
      mode: "same-origin",
      headers: {
        Accept: "application/json",
        "Content-type": "application/json",
        "X-CSRFToken": csrftoken,
      },
      body: JSON.stringify(body),
    });
    setEmail("");
    setModelChoice([]);
  };

  const handleClose = (event, reason, alertFunction) => {
    if (reason === "clickaway") {
      return;
    }
    alertFunction(false);
  };

  return (
    <Container maxWidth="md" className={classes.mainContent}>
      <Paper className={classes.paper}>
        <Typography variant="h4">Notifications</Typography>
        <Typography>
          Subscribe for email notification when chosen card will become
          available.
        </Typography>
        <Grid
          direction="column"
          container
          alignItems="flex-start"
          className={classes.grid}
        >
          <Grid item>
            <FormControl>
              <TextField
                required
                value={email}
                label="Email address"
                type="email"
                onChange={(e) => setEmail(e.target.value)}
                className={classes.formControl}
              />
            </FormControl>
          </Grid>
          <Grid item>
            <Autocomplete
              className={classes.modelChoice}
              multiple
              value={modelChoice}
              onChange={(e, newVal) => setModelChoice(newVal)}
              options={products}
              getOptionLabel={(option) => option.name}
              renderInput={(params) => (
                <TextField
                  {...params}
                  variant="standard"
                  label="Multiple values"
                  placeholder="Favorites"
                />
              )}
            />
          </Grid>
          <Grid item>
            <Button
              variant="contained"
              color="primary"
              className={classes.formControl}
              onClick={createNotificationRequest}
            >
              Submit
            </Button>
            <Snackbar
              open={openSuccess}
              autoHideDuration={5000}
              onClose={(event, reason) =>
                handleClose(event, reason, setOpenSuccess)
              }
            >
              <Alert variant="filled" severity="success" elevation={6}>
                Notification request created!
              </Alert>
            </Snackbar>
            <Snackbar
              open={openError}
              autoHideDuration={5000}
              onClose={(event, reason) =>
                handleClose(event, reason, setOpenError)
              }
            >
              <Alert variant="filled" severity="error" elevation={6}>
                Error occurred while creating notification request!
              </Alert>
            </Snackbar>
          </Grid>
        </Grid>
      </Paper>
    </Container>
  );
};

export default Notifications;
