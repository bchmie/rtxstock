import React, { useEffect, useState } from "react";
import { Container, Typography } from "@material-ui/core";
import ProductTable from "./ProductTable";
import Filters from "./Filters";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles({
  mainContent: {
    paddingTop: 96,
    paddingBottom: 24,
  },
});

const ProductList = ({ products }) => {
  const defaults = {
    name: "",
    manufacturer: "",
    available: false,
    priceRange: [0, 20000],
  };

  const [rows, setRows] = useState([]);
  const [name, setName] = useState(defaults["name"]);
  const [manufacturer, setManufacturer] = useState(defaults["manufacturer"]);
  const [available, setAvailable] = useState(defaults["available"]);
  const [priceRange, setPriceRange] = useState(defaults["priceRange"]);
  const classes = useStyles();

  const filterRow = (row) => {
    return (
      row.name.toLowerCase().includes(name.toLowerCase()) &&
      row.manufacturer.name
        .toLowerCase()
        .includes(manufacturer.toLowerCase()) &&
      (!available || row.is_available) &&
      priceRange[0] <= row.price &&
      row.price <= priceRange[1]
    );
  };

  const filterRows = (rows) => rows.filter((row) => filterRow(row));

  useEffect(() => {
    setRows(filterRows(products));
  }, [products, name, manufacturer, available, priceRange]);

  const clearFilters = () => {
    setName(defaults["name"]);
    setManufacturer(defaults["manufacturer"]);
    setAvailable(defaults["available"]);
    setPriceRange(defaults["priceRange"]);
  };

  return (
    <Container maxWidth="lg" className={classes.mainContent}>
      <Typography variant="body1">
        This table represents scraped data of RTX 3080 GPU from various shops
        offerings.
      </Typography>
      <Filters
        name={name}
        setName={setName}
        manufacturer={manufacturer}
        setManufacturer={setManufacturer}
        available={available}
        setAvailable={setAvailable}
        priceRange={priceRange}
        setPriceRange={setPriceRange}
        clearFilters={clearFilters}
      />
      <ProductTable rows={rows} />
    </Container>
  );
};

export default ProductList;
