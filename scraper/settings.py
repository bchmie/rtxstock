import pathlib

BOT_NAME = "scraper"

SPIDER_MODULES = ["scraper.spiders"]
NEWSPIDER_MODULE = "scraper.spiders"

USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0"

ROBOTSTXT_OBEY = False

CONCURRENT_REQUESTS = 4

DOWNLOAD_DELAY = 2

DEFAULT_REQUEST_HEADERS = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Language": "en-US,en;q=0.5",
    "DNT": "1",
    "Connection": "keep-alive",
    "Upgrade-Insecure-Requests": "1",
}

ITEM_PIPELINES = {
    "scraper.pipelines.ValidatePipeline": 100,
    "scraper.pipelines.MorelePreprocessPipeline": 300,
    "scraper.pipelines.ProlinePreprocessPipeline": 310,
    "scraper.pipelines.XkomPreprocessPipeline": 320,
    "scraper.pipelines.KomputronikPreprocessPipeline": 330,
    "scraper.pipelines.Process3080Pipeline": 700,
}

FEEDS = {
    pathlib.Path("items.csv"): {
        "format": "csv",
    }
}

FEED_EXPORTERS = {
    "csv": "scraper.exporters.NoHeaderCsvItemExporter",
}

FEED_EXPORT_FIELDS = [
    "model",
    "name",
    "manufacturer",
    "is_available",
    "price",
    "shop",
    "identifier",
    "timestamp",
    "link",
]

LOG_FILE = pathlib.Path("scraper.log")
LOG_LEVEL = "DEBUG"
