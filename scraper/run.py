from collections import defaultdict

from scrapy import signals
from scrapy.crawler import CrawlerRunner
from scrapy.signalmanager import dispatcher
from scrapy.utils.project import get_project_settings
from twisted.internet import reactor

from scraper.spiders.rtx3080 import (
    KomputronikSpider,
    MoreleSpider,
    ProlineSpider,
    XkomSpider,
)


def run():
    runner = CrawlerRunner(get_project_settings())
    runner.crawl(MoreleSpider)
    runner.crawl(XkomSpider)
    runner.crawl(ProlineSpider)
    runner.crawl(KomputronikSpider)
    d = runner.join()
    d.addBoth(lambda _: reactor.stop())
    reactor.run()


def run_and_return_items():
    results = defaultdict(list)

    def collect_result(item, response, spider):
        results[spider.name].append(item)

    dispatcher.connect(collect_result, signal=signals.item_scraped)
    run()

    return results


if __name__ == "__main__":
    run()
