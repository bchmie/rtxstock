import re
from datetime import datetime
from decimal import Decimal
from typing import Optional

from itemadapter import ItemAdapter
from scrapy.exceptions import DropItem


def process_price(price: Optional[str]) -> Optional[Decimal]:
    """Strips price text from unnecessary symbols and returns Decimal instance."""
    if price is None:
        return None
    return Decimal(price.rstrip("zł").replace(" ", "").replace(",", "."))


def normalize_nbsp(text: Optional[str]) -> Optional[str]:
    """Changes non-breaking spaces in text to usual spaces."""
    if text is None:
        return None
    return text.replace("\u00A0", " ")


def extract_from_brackets(text: Optional[str]) -> Optional[str]:
    """Returns text from between braces, otherwise does not modify text."""
    if text is None:
        return None
    return re.search(r"\[(.*)]", text).group(1)


class ValidatePipeline:
    """Class for initial object validation."""

    def process_item(self, item, spider):
        """Raises if item is missing `model` field."""
        adapter = ItemAdapter(item)
        if not adapter.get("model"):
            raise DropItem("Unrecognized product model")
        if not adapter.get("price"):
            raise DropItem("Item price is missing")
        return item


class MorelePreprocessPipeline:
    """Class for preprocessing items from morele spider."""

    def process_item(self, item, spider):
        """Preprocess item fields to make them compatible with 3080 pipeline."""
        if spider.name != "morele":
            return item
        adapter = ItemAdapter(item)
        name = adapter["name"]
        manufacturer = adapter["manufacturer"].strip()

        name = name.split(manufacturer, maxsplit=1)[1]
        name = name.rsplit("(", maxsplit=1)[0].strip()
        adapter["name"] = name
        return item


class ProlinePreprocessPipeline:
    """Class for preprocessing items from proline spider."""

    def process_item(self, item, spider):
        """Preprocess item fields to make them compatible with 3080 pipeline."""
        if spider.name != "proline":
            return item
        adapter = ItemAdapter(item)
        name = adapter["name"]
        manufacturer = adapter["manufacturer"]

        name = name.split(manufacturer, maxsplit=1)[1]
        name = name.rsplit("(", maxsplit=1)[0].strip()
        adapter["name"] = name
        return item


class XkomPreprocessPipeline:
    """Class for preprocessing items from x-kom spider."""

    def process_item(self, item, spider):
        """Preprocesses item fields to make the compatible with 3080 pipeline."""
        if spider.name != "x-kom":
            return item
        adapter = ItemAdapter(item)
        manufacturer = adapter.get("name").split("GeForce", maxsplit=1)[0].strip()
        name = adapter.get("name").split(manufacturer, maxsplit=1)[1].strip()
        adapter["manufacturer"] = manufacturer
        adapter["name"] = name
        return item


class KomputronikPreprocessPipeline:
    """Class for preprocessing items from komputronik spider."""

    def process_item(self, item, spider):
        """Preprocess item fields to make them compatible with 3080 pipeline."""
        if spider.name != "komputronik":
            return item
        adapter = ItemAdapter(item)
        adapter["model"] = extract_from_brackets(adapter.get("model"))
        manufacturer = adapter.get("manufacturer").strip()
        _, name = re.split(manufacturer, adapter["name"], maxsplit=1, flags=re.I)
        adapter["manufacturer"] = manufacturer
        adapter["name"] = name.strip()
        adapter["is_available"] = not bool(adapter.get("is_available"))
        adapter["identifier"] = extract_from_brackets(adapter.get("identifier"))
        adapter["price"] = normalize_nbsp(adapter["price"])
        return item


class Process3080Pipeline:
    """Class for processing generic 3080 items."""

    def process_item(self, item, spider):
        """Process scraped 3080 products."""
        adapter = ItemAdapter(item)
        adapter["price"] = process_price(adapter.get("price"))
        adapter["is_available"] = bool(adapter.get("is_available"))
        adapter["shop"] = spider.name
        adapter["timestamp"] = datetime.now().isoformat()
        return item
