from itemloaders.processors import TakeFirst
from scrapy import Field, Item


class ProductInfo(Item):
    """Class holding scraped product information"""

    model = Field(output_processor=TakeFirst())
    name = Field(output_processor=TakeFirst())
    manufacturer = Field(output_processor=TakeFirst())
    is_available = Field(output_processor=TakeFirst())
    price = Field(output_processor=TakeFirst())
    shop = Field(output_processor=TakeFirst())
    identifier = Field(output_processor=TakeFirst())
    timestamp = Field(output_processor=TakeFirst())
    link = Field(output_processor=TakeFirst())
