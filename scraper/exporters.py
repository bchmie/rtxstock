from scrapy.exporters import CsvItemExporter


class NoHeaderCsvItemExporter(CsvItemExporter):
    """CsvItemExporter subclass that does not log header"""

    def __init__(self, file, join_multivalued=",", errors=None, **kwargs):
        super(NoHeaderCsvItemExporter, self).__init__(
            file=file,
            include_headers_line=False,
            join_multivalued=join_multivalued,
            errors=errors,
            **kwargs
        )
