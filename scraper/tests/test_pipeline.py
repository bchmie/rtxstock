from decimal import Decimal
from unittest.mock import Mock

import pytest
from scrapy.exceptions import DropItem

from scraper.pipelines import (
    KomputronikPreprocessPipeline,
    MorelePreprocessPipeline,
    Process3080Pipeline,
    ProlinePreprocessPipeline,
    ValidatePipeline,
    XkomPreprocessPipeline,
    extract_from_brackets,
    normalize_nbsp,
    process_price,
)


class TestProcessPrice:
    def test_handles_none_value(self):
        result = process_price(None)
        assert result is None

    def test_removes_currency_suffix(self):
        result = process_price("1000 zł")
        assert result == Decimal(1000)

    def test_normalizes_whitespace_in_price(self):
        result = process_price(" 1 000  000 ")
        assert result == Decimal(1000000)

    def test_replaces_commas(self):
        result = process_price("1000,99")
        assert result == Decimal("1000.99")

    def test_complex_handling(self):
        result = process_price("1 000,99 zł")
        assert result == Decimal("1000.99")


class TestNormalizeNbsp:
    def test_handles_none(self):
        result = normalize_nbsp(None)
        assert result is None

    def test_replaces_non_breaking_space_with_normal_space(self):
        result = normalize_nbsp("1\u00A0000,99 zł")
        assert result == "1 000,99 zł"


class TestExtractFromBrackets:
    def test_handles_none(self):
        result = extract_from_brackets(None)
        assert result is None

    def test_handles_brackets(self):
        result = extract_from_brackets("[123]")
        assert result == "123"

    def test_handles_text_before_brackets(self):
        result = extract_from_brackets("Outside brackets: [123]")
        assert result == "123"


class TestValidatePipeline:
    def test_drops_items_without_specified_model(self):
        with pytest.raises(DropItem):
            ValidatePipeline().process_item(item={"model": ""}, spider=None)

    def test_drops_items_without_specified_price(self):
        with pytest.raises(DropItem):
            ValidatePipeline().process_item(item={"price": None}, spider=None)

    def test_passes_items_with_model(self):
        item = {"model": "item model", "price": "5999.00"}
        result = ValidatePipeline().process_item(item, spider=None)
        assert result is item


class TestMorelePreprocessPipeline:
    pipeline = MorelePreprocessPipeline()
    spider = Mock()
    item = {
        "model": "[item model]",
        "name": "Prefix Manufacturer Item Name (some specifier)",
        "manufacturer": "Manufacturer",
    }

    def test_does_not_process_items_from_other_spiders(self):
        self.spider.name = "not morele"
        result = self.pipeline.process_item(item=dict(**self.item), spider=self.spider)
        assert result == self.item

    def test_does_processes_items_from_komputronik_spider(self):
        self.spider.name = "morele"
        result = self.pipeline.process_item(item=dict(**self.item), spider=self.spider)
        assert result["name"] == "Item Name"


class TestProlinePreprocessPipeline:
    pipeline = ProlinePreprocessPipeline()
    spider = Mock()
    item = {
        "model": "[item model]",
        "name": "Prefix Manufacturer Item Name (some specifier)",
        "manufacturer": "Manufacturer",
    }

    def test_does_not_process_items_from_other_spiders(self):
        self.spider.name = "not proline"
        result = self.pipeline.process_item(item=dict(**self.item), spider=self.spider)
        assert result == self.item

    def test_does_processes_items_from_komputronik_spider(self):
        self.spider.name = "proline"
        result = self.pipeline.process_item(item=dict(**self.item), spider=self.spider)
        assert result["name"] == "Item Name"


class TestXkomPreprocessPipeline:
    pipeline = XkomPreprocessPipeline()
    spider = Mock()
    item = {
        "model": "[item model]",
        "name": "Manufacturer GeForce GPU",
    }

    def test_does_not_process_items_from_other_spiders(self):
        self.spider.name = "not x-kom"
        result = self.pipeline.process_item(item=dict(**self.item), spider=self.spider)
        assert result == self.item

    def test_does_processes_items_from_komputronik_spider(self):
        self.spider.name = "x-kom"
        result = self.pipeline.process_item(item=dict(**self.item), spider=self.spider)
        assert result["name"] == "GeForce GPU"
        assert result["manufacturer"] == "Manufacturer"


class TestKomputronikPreprocessPipeline:
    pipeline = KomputronikPreprocessPipeline()
    spider = Mock()
    item = {
        "model": "[item model]",
        "name": "Manufacturer Item Name",
        "manufacturer": "Manufacturer",
        "is_available": "yes",
        "identifier": "[1]",
        "price": "1\u00A0000 zł",
    }

    def test_does_not_process_items_from_other_spiders(self):
        self.spider.name = "not komputronik"
        result = self.pipeline.process_item(item=dict(**self.item), spider=self.spider)
        assert result == self.item

    def test_does_processes_items_from_komputronik_spider(self):
        self.spider.name = "komputronik"
        result = self.pipeline.process_item(item=dict(**self.item), spider=self.spider)
        assert result["model"] == "item model"
        assert result["name"] == "Item Name"
        assert isinstance(result["is_available"], bool)
        assert result["identifier"] == "1"
        assert result["price"] == "1 000 zł"


class TestProcess3080Pipeline:
    pipeline = Process3080Pipeline()
    spider = Mock()
    item = {
        "model": "item model",
        "is_available": "yes",
        "identifier": "1",
        "price": "1 000,99 zł",
    }

    def test_does_process_items(self):
        self.spider.name = "morele"
        result = self.pipeline.process_item(item=dict(**self.item), spider=self.spider)
        assert result["price"] == Decimal("1000.99")
        assert isinstance(result["is_available"], bool)
        assert result["shop"] == self.spider.name
        assert isinstance(result["timestamp"], str)
