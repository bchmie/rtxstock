import scrapy
from scrapy.loader import ItemLoader

from scraper.items import ProductInfo


class MoreleSpider(scrapy.Spider):
    """Spider for scraping RTX 3080 product data from morele.net shop."""

    name = "morele"
    allowed_domains = ["morele.net"]
    start_urls = [
        "https://www.morele.net/kategoria/karty-graficzne-12/,,1,,,,,,0,,,,8143O1689340/"
    ]

    def parse(self, response, **_):
        products = response.css("a.productLink")
        yield from response.follow_all(products, callback=self.parse_product)

    def parse_product(self, response):
        loader = ItemLoader(item=ProductInfo(), response=response)
        loader.add_xpath(
            "model",
            "(//div[div[normalize-space(text()) = 'Kod producenta']]/div)[2]/div/text()",
        )
        loader.add_css("name", ".prod-name::text")
        loader.add_xpath(
            "manufacturer",
            "(//div[div[normalize-space(text()) = 'Producent']]/div)[2]/div/a/span/text()",
        )
        loader.add_xpath("identifier", "//*[@itemprop='gtin8']/text()")
        loader.add_xpath("price", "//div[@id='product_price_brutto']/@content")
        loader.add_css("is_available", "div.add-to-cart")
        loader.add_value("link", response.url)
        yield loader.load_item()


class XkomSpider(scrapy.Spider):
    """Spider for scraping RTX 3080 product data from x-kom.pl shop."""

    name = "x-kom"
    allowed_domains = ["x-kom.pl"]
    start_urls = [
        r"https://www.x-kom.pl/g-5/c/346-karty-graficzne-nvidia.html?f[1702][178106]=1"
    ]

    def parse(self, response, **_):
        products = response.xpath("//div[@id='listing-container']//a")
        yield from response.follow_all(products, callback=self.parse_product)

    def parse_product(self, response):
        loader = ItemLoader(item=ProductInfo(), response=response)
        loader.add_xpath(
            "model",
            "//div[div[normalize-space(text()) = 'Kod producenta']]/following-sibling::div/div/text()",
        )
        loader.add_xpath("name", "(//h1)[1]/text()")
        loader.add_xpath(
            "identifier",
            "//div[div[normalize-space(text()) = 'Kod x-kom']]/following-sibling::div/div/text()",
        )
        loader.add_css("price", ".n4n86h-4::text")
        loader.add_css("is_available", ".hQyNnf")
        loader.add_value("link", response.url)
        yield loader.load_item()


class ProlineSpider(scrapy.Spider):
    """Spider for scraping RTX 3080 product data from proline.pl shop."""

    name = "proline"
    allowed_domains = ["proline.pl"]
    start_urls = [r"https://proline.pl/?g=Karty+graficzne&c_chipset-model=rtx+3080"]

    def parse(self, response, **_):
        products = response.css("a.produkt.pbig")
        yield from response.follow_all(products, callback=self.parse_product)

    def parse_product(self, response):
        loader = ItemLoader(item=ProductInfo(), response=response)
        loader.add_xpath(
            "model", "//span[@title='Identyfikator kod ID Produktu']/b/text()"
        )
        loader.add_css("name", ".prodid::text")
        loader.add_xpath(
            "manufacturer", "//td[text()='Producent']/following-sibling::td/text()"
        )
        loader.add_xpath(
            "price",
            "//table[@class = 'cenaline_karta']//b[@class = 'cena_b']/span//text()",
        )
        loader.add_css("is_available", "div.dostepnosc_info.green")
        loader.add_value("link", response.url)
        yield loader.load_item()


class KomputronikSpider(scrapy.Spider):
    """Spider for scraping RTX 3080 product data from komputronik.pl shop."""

    name = "komputronik"
    allowed_domains = ["komputronik.pl"]
    start_urls = [
        r"https://www.komputronik.pl/search/category/1099?query=%22RTX%203080%22"
    ]

    def parse(self, response, **_):
        products = response.xpath("//a[contains(@class, 'at-product-name')]")
        yield from response.follow_all(products, callback=self.parse_product)

    def parse_product(self, response):
        loader = ItemLoader(item=ProductInfo(), response=response)
        loader.add_xpath(
            "model",
            "//div[@class='codes']/ul/li[2][not(contains(text(), '$ctrl'))]/text()",
        )
        loader.add_xpath("name", "//section[@id='p-inner-name']/h1/text()")
        loader.add_xpath(
            "manufacturer",
            "//th[text()='Producent']/following-sibling::td/a[not(contains(text(), '$ctrl'))]/text()",
        )
        loader.add_xpath(
            "identifier",
            "//div[@class='codes']/ul/li[3][not(contains(text(), '$ctrl'))]/text()",
        )
        loader.add_xpath(
            "price", "//div[@class='prices']//span[@class='proper']/text()"
        )
        loader.add_xpath(
            "is_available",
            "//section[@id='p-inner-delivery']//a[not(contains(text(), 'Produkt tymczasowo niedostępny'))]",
        )
        loader.add_value("link", response.url)
        yield loader.load_item()
