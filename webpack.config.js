const Dotenv = require("dotenv-webpack");
const path = require("path");
const webpack = require("webpack");

module.exports = {
  context: path.resolve(__dirname, "frontend"),
  entry: "./index.js",
  output: {
    filename: "index-bundle.js",
    path: path.resolve(__dirname, "./frontend/static"),
  },
  devServer: {
    static: "./frontend/static",
    allowedHosts: ["frontend"],
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        options: {
          presets: ["@babel/preset-env", "@babel/preset-react"],
          plugins: ["@babel/plugin-transform-runtime"],
        },
      },
    ],
  },
  plugins: [
    new webpack.EnvironmentPlugin({
      API_ENDPOINT: JSON.stringify(process.env.API_ENDPOINT),
    }),
  ],
};
