from django.contrib import admin

from rtxstock.models import Manufacturer, Offer, Product, Shop

admin.site.register(Manufacturer)
admin.site.register(Product)
admin.site.register(Shop)
admin.site.register(Offer)
