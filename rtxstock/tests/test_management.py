import datetime

import pytest
from django.core.management import call_command

from rtxstock.factories import OfferFactory
from rtxstock.models import Offer, Statistics


@pytest.mark.django_db
class TestPopulateStatistics:
    def test_handle_create_statistics_instances(self, yesterday):
        day_before = yesterday - datetime.timedelta(days=1)
        OfferFactory(timestamp=yesterday)
        OfferFactory(timestamp=day_before)
        call_command("populatestatistics")
        assert Statistics.objects.count() == 2


@pytest.mark.django_db
class TestLoadOffers:
    def test_load_offers(self, csv_file):
        call_command("loadoffers", csv_file)
        assert Offer.objects.count() == 3
