import datetime
from decimal import Decimal

import pytest
from django.utils import timezone
from rest_framework.test import APIClient

from rtxstock.factories import ManufacturerFactory, ProductFactory, ShopFactory


@pytest.fixture
def manufacturer():
    return ManufacturerFactory()


@pytest.fixture
def product():
    return ProductFactory()


@pytest.fixture
def shop():
    return ShopFactory()


@pytest.fixture
def scrapy_item():
    return {
        "model": "product_model",
        "name": "Product Name",
        "manufacturer": "Manufacturer",
        "shop": "shop",
        "identifier": "1234asdf",
        "price": Decimal("1000"),
        "is_available": False,
        "timestamp": datetime.datetime.now().isoformat(),
        "link": "https://product.example",
    }


@pytest.fixture
def client():
    return APIClient()


@pytest.fixture
def yesterday():
    return timezone.now() - datetime.timedelta(days=1)


@pytest.fixture
def csv_file(tmpdir):
    csv_file = tmpdir.join("items.csv")
    csv_file.write(
        "RTX 3080 GAMING X TRIO 10G,GeForce RTX 3080 Gaming X Trio 10GB GDDR6X,MSI,False,7499.00,morele,,"
        "2021-04-05 14:30:59.914207,https://www.morele.net/karta-graficzna-msi-geforce-rtx-3080-gaming-x-"
        "trio-10gb-gddr6x-rtx-3080-gaming-x-trio-10g-5943771/\n"
    )
    csv_file.write(
        "GV-N3080GAMING OC-10GD,GeForce RTX 3080 Gaming OC 10GB GDDR6X,Gigabyte,False,8989.00,morele,,"
        "2021-04-05 14:30:57.570445,https://www.morele.net/karta-graficzna-gigabyte-geforce-rtx-3080-g"
        "aming-oc-10gb-gddr6x-gv-n3080gaming-oc-10gd-5943775/\n",
        mode="a",
    )
    csv_file.write(
        "GV-N3080EAGLE OC-10GD,GeForce RTX 3080 Eagle OC 10GB GDDR6X,Gigabyte,False,4799.00,morele,,"
        "2021-04-05 14:30:55.894471,https://www.morele.net/karta-graficzna-gigabyte-geforce-rtx-3080"
        "-eagle-oc-10gb-gddr6x-gv-n3080eagle-oc-10gd-5943776/",
        mode="a",
    )
    return csv_file
