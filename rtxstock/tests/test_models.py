import datetime
from decimal import Decimal

import pytest
from django.core.exceptions import ValidationError
from django.utils import timezone

from rtxstock.factories import (
    OfferFactory,
    ProductFactory,
    ShopFactory,
    StatisticsFactory,
)
from rtxstock.models import Offer, Product, Shop, Statistics


@pytest.mark.django_db
class TestProduct:
    def test_check_availability_no_offers(self, product):
        assert not product.check_availability()

    def test_check_availability_not_available(self, product):
        OfferFactory(product=product, is_available=False)
        assert not product.check_availability()

    def test_check_availability_available(self, product):
        OfferFactory(product=product, is_available=True)
        OfferFactory(product=product, is_available=False)
        assert product.check_availability()

    def test_update_product_updates_last_seen(self, product):
        created_timestamp = product.last_seen
        offer = OfferFactory(product=product, is_available=True)
        assert product.last_seen != created_timestamp
        assert product.last_seen == offer.timestamp

    def test_update_product_goes_available(self, product):
        assert not product.is_available
        offer = OfferFactory(product=product, is_available=True)
        assert product.is_available
        assert product.last_available == offer.timestamp

    def test_update_product_goes_unavailable(self):
        product = ProductFactory(is_available=True)
        assert product.is_available
        OfferFactory(product=product, is_available=False)
        assert not product.is_available

    def test_update_product_remains_available(self, product):
        OfferFactory(product=product, is_available=True)
        assert product.is_available
        OfferFactory(product=product, is_available=False)
        assert product.is_available


@pytest.mark.django_db
class TestShop:
    def test_last_updated_returns_shops_latest_recorded_offer(self, shop):
        offer = OfferFactory(shop=shop)
        OfferFactory()
        assert shop.last_updated == offer.timestamp

    def test_last_updated_returns_none_if_no_offers(self, shop):
        assert shop.last_updated is None


@pytest.mark.django_db
class TestOfferManager:
    def test_create_from_scrapy_item_creates_offer_and_related_items(self, scrapy_item):
        offer = Offer.objects.create_from_scrapy_item(scrapy_item)
        assert Offer.objects.first() == offer
        assert Product.objects.count() == 1
        assert Shop.objects.count() == 1

    def test_create_from_scrapy_item_creates_offer_with_existing_related_items(
        self, scrapy_item
    ):
        product = ProductFactory()
        shop = ShopFactory()
        scrapy_item.update(model=product.model, shop=shop.name)
        offer = Offer.objects.create_from_scrapy_item(scrapy_item)
        assert offer.product == product
        assert offer.shop == shop

    def test_bulk_create_from_scrapy_items(self, scrapy_item):
        another_item = dict(**scrapy_item)
        another_item.update(model="another_model")
        offers = Offer.objects.bulk_create_from_scrapy_items(
            [scrapy_item, another_item]
        )
        assert list(Offer.objects.all()) == offers

    def test_price_history_returns_one_offer_per_day_for_same_shop(self, product, shop):
        OfferFactory.create_batch(2, product=product, shop=shop)
        result = product.price_history(days=7)
        assert len(result) == 1

    def test_price_history_returns_one_offer_per_day_for_different_shop(
        self, product, shop
    ):
        OfferFactory(product=product, shop=shop)
        OfferFactory(product=product, shop=ShopFactory())
        result = product.price_history(days=7)
        assert len(result) == 2


@pytest.mark.django_db
class TestStatisticsManager:
    def test_create_for_date_raises_validation_error_for_today_date(self):
        with pytest.raises(ValidationError):
            Statistics.objects.create_for_date(timezone.now().date())

    def test_create_for_date_raises_validation_error_for_future_date(self):
        tomorrow = timezone.now().date() + datetime.timedelta(days=1)
        with pytest.raises(ValidationError):
            Statistics.objects.create_for_date(tomorrow)

    def test_create_for_date_calculates_mean_price(self, yesterday):
        OfferFactory(price=Decimal(1000), timestamp=yesterday)
        OfferFactory(price=Decimal(1000), timestamp=yesterday)
        OfferFactory(price=Decimal(10000), timestamp=yesterday)

        stats = Statistics.objects.create_for_date(yesterday)

        assert stats.mean_price == Decimal(4000)

    def test_create_for_date_calculates_median_price(self, yesterday):
        OfferFactory(price=Decimal(1000), timestamp=yesterday)
        OfferFactory(price=Decimal(1000), timestamp=yesterday)
        OfferFactory(price=Decimal(10000), timestamp=yesterday)

        stats = Statistics.objects.create_for_date(yesterday)

        assert stats.median_price == Decimal(1000)

    def test_create_for_date_does_count_available_cards(self, yesterday):
        OfferFactory.create_batch(10, is_available=True, timestamp=yesterday)
        stats = Statistics.objects.create_for_date(yesterday)
        assert stats.cards_available == 10

    def test_create_for_date_does_not_count_cards_from_earlier_dates(self, yesterday):
        day_before = yesterday - datetime.timedelta(days=1)
        OfferFactory.create_batch(3, is_available=True, timestamp=day_before)
        OfferFactory(is_available=False, timestamp=yesterday)

        stats = Statistics.objects.create_for_date(yesterday)

        assert stats.cards_available == 0

    def test_create_for_date_does_not_count_cards_from_further_dates(self, yesterday):
        day_before = yesterday - datetime.timedelta(days=1)
        OfferFactory(is_available=False, timestamp=yesterday)
        OfferFactory.create_batch(3, is_available=True, timestamp=day_before)

        stats = Statistics.objects.create_for_date(yesterday)

        assert stats.cards_available == 0

    def test_create_for_date_does_raise_validation_error_if_no_datapoints(
        self, yesterday
    ):
        with pytest.raises(ValidationError):
            Statistics.objects.create_for_date(yesterday)

    def test_create_for_date_updates_stats_already_existing_for_given_date(
        self, yesterday
    ):
        OfferFactory(is_available=True, timestamp=yesterday)
        stats = Statistics.objects.create_for_date(yesterday)
        assert stats.cards_available == 1

        OfferFactory(is_available=True, timestamp=yesterday)
        stats = Statistics.objects.create_for_date(yesterday)
        assert stats.cards_available == 2

    def test_get_dates_with_missing_stats_finds_missing_dates(self, yesterday):
        OfferFactory(is_available=True, timestamp=yesterday)
        missing = Statistics.objects.get_dates_with_missing_stats()
        assert missing.pop() == yesterday.date()

    def test_get_dates_with_missing_stats_does_not_report_days_with_stats(
        self, yesterday
    ):
        OfferFactory(is_available=True, timestamp=yesterday)
        StatisticsFactory(date=yesterday)
        missing = Statistics.objects.get_dates_with_missing_stats()
        assert len(missing) == 0
