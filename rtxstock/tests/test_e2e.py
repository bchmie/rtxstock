import pytest
from rest_framework.reverse import reverse

from rtxstock.factories import (
    ManufacturerFactory,
    OfferFactory,
    ProductFactory,
    ShopFactory,
    StatisticsFactory,
)
from rtxstock.serializers import (
    ManufacturerSerializer,
    OfferSerializer,
    ProductSerializer,
    ShopSerializer,
    StatisticsSerializer,
)


@pytest.mark.django_db
class TestE2E:
    def test_list_manufacturers(self, client):
        manufacturers = ManufacturerFactory.create_batch(3)
        response = client.get(reverse("rtxstock:manufacturers-list"))
        for manufacturer in ManufacturerSerializer(manufacturers, many=True).data:
            assert manufacturer in response.data

    def test_detail_manufacturers(self, client):
        manufacturer = ManufacturerFactory()
        response = client.get(
            reverse("rtxstock:manufacturers-detail", args=(manufacturer.id,))
        )
        assert response.data == ManufacturerSerializer(manufacturer).data

    def test_list_products(self, client):
        products = ProductFactory.create_batch(3)
        response = client.get(reverse("rtxstock:products-list"))
        response_models = [item["model"] for item in response.data]
        for product in ProductSerializer(products, many=True).data:
            assert product["model"] in response_models

    def test_detail_products(self, client):
        product = ProductFactory()
        response = client.get(
            reverse("rtxstock:products-detail", args=(product.model,))
        )
        assert response.data == ProductSerializer(product).data

    def test_detail_product_offers(self, client):
        product = ProductFactory()
        offers = OfferFactory.create_batch(10, product=product)
        response = client.get(
            reverse("rtxstock:products-detail-offers", args=(product.model,))
        )
        assert response.status_code == 200
        for offer in OfferSerializer(offers, many=True).data:
            assert offer in response.data["results"]

    def test_detail_product_offers_does_not_return_other_offers(self, client):
        product = ProductFactory()
        OfferFactory.create_batch(10)
        response = client.get(
            reverse("rtxstock:products-detail-offers", args=(product.model,))
        )
        assert response.status_code == 200
        assert len(response.data["results"]) == 0

    def test_list_shops(self, client):
        shops = ShopFactory.create_batch(3)
        response = client.get(reverse("rtxstock:shops-list"))
        for shop in ShopSerializer(shops, many=True).data:
            assert shop in response.data

    def test_detail_shops(self, client):
        shop = ShopFactory()
        response = client.get(reverse("rtxstock:shops-detail", args=(shop.id,)))
        assert response.data == ShopSerializer(shop).data

    def test_list_offers(self, client):
        offers = OfferFactory.create_batch(3)
        response = client.get(reverse("rtxstock:offers-list"))
        for offer in OfferSerializer(offers, many=True).data:
            assert offer in response.data["results"]

    def test_detail_offers(self, client):
        offer = OfferFactory()
        response = client.get(reverse("rtxstock:offers-detail", args=(offer.id,)))
        assert response.data == OfferSerializer(offer).data

    def test_list_statistics(self, client):
        stats = StatisticsFactory.create_batch(3)
        response = client.get(reverse("rtxstock:statistics-list"))
        for stat in StatisticsSerializer(stats, many=True).data:
            assert stat in response.data

    def test_get_product_price_history(self, client):
        product = ProductFactory()
        offer = OfferFactory(product=product)
        response = client.get(
            reverse("rtxstock:products-price-history", args=(product.model, 7))
        )
        assert response.status_code == 200
        for result in OfferSerializer([offer], many=True).data:
            assert result in response.data
