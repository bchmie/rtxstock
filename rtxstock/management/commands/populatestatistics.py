from django.core.management import BaseCommand

from rtxstock.models import Statistics


class Command(BaseCommand):
    help = "Creates statistics for days that have scraped offers but no statistics calculated"

    def handle(self, *args, **options):
        missing = Statistics.objects.get_dates_with_missing_stats()
        for day in missing:
            Statistics.objects.create_for_date(day)
