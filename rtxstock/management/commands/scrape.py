from django.core.management import BaseCommand

from rtxstock.models import Offer
from scraper.run import run_and_return_items


class Command(BaseCommand):
    help = "Starts spiders scraping shops for 3080 data"

    def handle(self, *args, **options):
        results = run_and_return_items()
        for shop, items in results.items():
            Offer.objects.bulk_create_from_scrapy_items(items)
