import csv
import decimal
import os

from django.core.management import BaseCommand

from rtxstock.models import Offer


def csv_feed(path):
    with open(path) as csvfile:
        reader = csv.reader(csvfile)
        yield from reader


class Command(BaseCommand):
    help = "Loads offers data from a csv file"

    def add_arguments(self, parser):
        parser.add_argument("csv_file", type=str)

    def handle(self, *args, **options):
        filepath = options["csv_file"]
        abspath = os.path.abspath(filepath)
        dropped_items = []
        items_digested = 0
        for row in csv_feed(abspath):
            try:
                item = {
                    "model": row[0],
                    "name": row[1],
                    "manufacturer": row[2],
                    "is_available": row[3] == "True",
                    "price": decimal.Decimal(row[4]),
                    "shop": row[5],
                    "identifier": row[6],
                    "timestamp": row[7],
                    "link": row[8],
                }
                Offer.objects.create_from_scrapy_item(item)
                items_digested += 1
            except decimal.InvalidOperation:
                dropped_items.append(row)
        print(f"Number of items read from csv: {items_digested}")
        print(f"Number of dropped items: {len(dropped_items)}")
        print(f"Dropped items: {dropped_items}")
