import datetime
import statistics
from decimal import Decimal

from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Min
from django.db.models.functions import TruncDay
from django.utils import timezone
from django.utils.timezone import is_naive, make_aware
from simple_history.models import HistoricalRecords


class Manufacturer(models.Model):
    name = models.CharField(max_length=120)


class ProductManager(models.Manager):
    def with_prices(self):
        return self.annotate(price=Min("offers__price"))


class Product(models.Model):
    model = models.CharField(max_length=120, primary_key=True)
    name = models.CharField(max_length=120, blank=True, null=True)
    trade_item_number = models.CharField(max_length=120, blank=True, null=True)
    manufacturer = models.ForeignKey(
        Manufacturer, blank=True, null=True, on_delete=models.SET_NULL
    )
    msrp = models.DecimalField(max_digits=7, decimal_places=2, blank=True, null=True)
    is_available = models.BooleanField(default=False)
    last_seen = models.DateTimeField(null=True)
    last_available = models.DateTimeField(null=True)

    objects = ProductManager()

    def check_availability(self):
        return self.offers.filter(is_available=True).exists()

    def update_product(self, offer):
        self.last_seen = offer.timestamp
        if offer.is_available:
            self.is_available = True
            self.last_available = offer.timestamp
        else:
            self.is_available = self.check_availability()
        self.save()

    def price_history(self, days):
        """Returns daily offers for given product."""
        from_timestamp = timezone.now() - datetime.timedelta(days=days)
        return (
            Offer.history.filter(product=self, timestamp__gte=from_timestamp)
            .annotate(day=TruncDay("timestamp"))
            .order_by("day", "shop", "timestamp")
            .distinct("day", "shop")
        )

    def __str__(self):
        return self.model


class Shop(models.Model):
    name = models.CharField(max_length=120)

    @property
    def last_updated(self):
        """Returns timestamp of when shop was last updated."""
        try:
            last_offer = self.offers.latest()
        except Offer.DoesNotExist:
            return None
        return last_offer.timestamp

    def __str__(self):
        return self.name


class OfferManager(models.Manager):
    def create_from_scrapy_item(self, scrapy_item):
        """Creates Offer item from Scrapy ProductInfo item."""
        timestamp = datetime.datetime.fromisoformat(scrapy_item["timestamp"])
        if is_naive(timestamp):
            timestamp = make_aware(timestamp)
        manufacturer, _ = Manufacturer.objects.get_or_create(
            name=scrapy_item["manufacturer"]
        )
        defaults = {
            "name": scrapy_item["name"],
            "manufacturer": manufacturer,
            "is_available": scrapy_item["is_available"],
            "last_seen": timestamp,
        }
        identifier = scrapy_item.get("identifier")
        if identifier:
            defaults["trade_item_number"] = identifier

        product, _ = Product.objects.get_or_create(
            model=scrapy_item["model"], defaults=defaults
        )
        shop, _ = Shop.objects.get_or_create(name=scrapy_item["shop"])
        offer, _ = self.update_or_create(
            product=product,
            shop=shop,
            defaults={
                "price": Decimal(scrapy_item["price"]),
                "is_available": bool(scrapy_item["is_available"]),
                "timestamp": timestamp,
                "link": scrapy_item["link"],
            },
        )
        return offer

    def bulk_create_from_scrapy_items(self, items):
        """Creates Offer items from list of ProductInfo items."""
        return [self.create_from_scrapy_item(item) for item in items]


class Offer(models.Model):

    product = models.ForeignKey(
        Product, related_name="offers", null=True, on_delete=models.SET_NULL
    )
    shop = models.ForeignKey(
        Shop, related_name="offers", null=True, on_delete=models.SET_NULL
    )
    price = models.DecimalField(max_digits=7, decimal_places=2)
    is_available = models.BooleanField()
    timestamp = models.DateTimeField()
    link = models.URLField()

    history = HistoricalRecords(related_name="previous_offers")
    objects = OfferManager()

    class Meta:
        ordering = ["-timestamp"]
        get_latest_by = ["timestamp"]

    def save(self, *args, **kwargs):
        super(Offer, self).save(*args, **kwargs)
        if self.timestamp > self.product.last_seen:
            self.product.update_product(self)

    def __str__(self):
        return f"{self.shop}: {self.product}"


class StatisticsManager(models.Manager):
    def create_for_date(self, date):
        """Creates or updates instance of Statistics model for given date."""
        if isinstance(date, datetime.datetime):
            date = date.date()
        if date >= timezone.now().date():
            raise ValidationError(
                f"{date=} is today or in future, unable to calculate stats for such date"
            )
        price_list = (
            Offer.history.filter(timestamp__date=date)
            .order_by("product")
            .distinct("product")
            .values_list("price", flat=True)
        )
        if len(price_list) == 0:
            raise ValidationError(
                "There are no datapoints to calculate statistics from."
            )
        mean_price = statistics.mean(price_list)
        median_price = statistics.median(price_list)

        cards_available = (
            Offer.history.filter(timestamp__date=date, is_available=True)
            .order_by("product")
            .distinct("product")
            .count()
        )
        stats, _ = self.update_or_create(
            date=make_aware(
                datetime.datetime.combine(date, datetime.datetime.min.time())
            ),
            defaults={
                "cards_available": cards_available,
                "mean_price": mean_price,
                "median_price": median_price,
            },
        )
        return stats

    def get_dates_with_missing_stats(self):
        """Returns list of dates for which statistics are not calculated"""
        days_with_offers = set(
            Offer.history.annotate(day=TruncDay("timestamp")).values_list(
                "day", flat=True
            )
        )
        days_with_stats = set(
            self.model.objects.annotate(day=TruncDay("date")).values_list(
                "day", flat=True
            )
        )
        today = timezone.now()
        today = today.replace(hour=0, minute=0, second=0, microsecond=0)
        difference = days_with_offers - days_with_stats - {today}
        return [day.date() for day in difference]


class Statistics(models.Model):

    date = models.DateTimeField(unique_for_date="date")
    cards_available = models.IntegerField()
    mean_price = models.DecimalField(max_digits=7, decimal_places=2)
    median_price = models.DecimalField(max_digits=7, decimal_places=2)

    objects = StatisticsManager()

    class Meta:
        ordering = ["-date"]
        get_latest_by = ["date"]

    def __str__(self):
        return (
            f"Statistics for {self.date}: cards available: {self.cards_available}, "
            f"mean_price: {self.mean_price}, median_price: {self.median_price}"
        )
