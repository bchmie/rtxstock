from django.shortcuts import get_object_or_404
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rest_framework.views import APIView

from rtxstock.models import Manufacturer, Offer, Product, Shop, Statistics
from rtxstock.serializers import (
    ManufacturerSerializer,
    OfferSerializer,
    ProductSerializer,
    ShopSerializer,
    StatisticsSerializer,
)


class PaginationMixin:
    @property
    def paginator(self):
        """
        The paginator instance associated with the view, or `None`.
        """
        if not hasattr(self, "_paginator"):
            if self.pagination_class is None:
                self._paginator = None
            else:
                self._paginator = self.pagination_class()
        return self._paginator

    def paginate_queryset(self, queryset):
        """
        Return a single page of results, or `None` if pagination is disabled.
        """
        if self.paginator is None:
            return None
        return self.paginator.paginate_queryset(queryset, self.request, view=self)

    def get_paginated_response(self, data):
        """
        Return a paginated style `Response` object for the given output data.
        """
        assert self.paginator is not None
        return self.paginator.get_paginated_response(data)


class ManufacturerListApi(APIView):
    def get(self, request):
        manufacturers = Manufacturer.objects.all()
        serializer = ManufacturerSerializer(manufacturers, many=True)
        return Response(serializer.data)


class ManufacturerDetailApi(APIView):
    def get(self, request, pk):
        manufacturer = get_object_or_404(Manufacturer, pk=pk)
        serializer = ManufacturerSerializer(manufacturer)
        return Response(serializer.data)


class ProductListApi(APIView):
    def get(self, request):
        products = (
            Product.objects.with_prices()
            .select_related("manufacturer")
            .prefetch_related("offers__shop")
        )
        serializer = ProductSerializer(products, many=True)
        return Response(serializer.data)


class ProductDetailApi(APIView):
    def get(self, request, model):
        product = get_object_or_404(Product, model=model)
        serializer = ProductSerializer(product)
        return Response(serializer.data)


class ProductDetailOffersApi(APIView, PaginationMixin):
    serializer_class = OfferSerializer
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS

    def get(self, request, model):
        product = get_object_or_404(Product, model=model)
        offers = Offer.objects.filter(product=product)
        page = self.paginate_queryset(offers)
        if page is not None:
            serializer = self.serializer_class(page, many=True)
            return self.get_paginated_response(serializer.data)


class ProductPriceHistory(APIView):
    def get(self, request, model, days):
        product = get_object_or_404(Product, model=model)
        offers = product.price_history(days)
        serializer = OfferSerializer(offers, many=True)
        return Response(serializer.data)


class ShopListApi(APIView):
    def get(self, request):
        shops = Shop.objects.all()
        serializer = ShopSerializer(shops, many=True)
        return Response(serializer.data)


class ShopDetailApi(APIView):
    def get(self, request, pk):
        shop = get_object_or_404(Shop, pk=pk)
        serializer = ShopSerializer(shop)
        return Response(serializer.data)


class OfferListApi(APIView, PaginationMixin):
    queryset = Offer.objects.all()
    serializer_class = OfferSerializer
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS

    def get(self, request):
        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.serializer_class(page, many=True)
            return self.get_paginated_response(serializer.data)


class OfferDetailApi(APIView):
    def get(self, request, pk):
        offer = get_object_or_404(Offer, pk=pk)
        serializer = OfferSerializer(offer)
        return Response(serializer.data)


class StatisticsListApi(APIView):
    def get(self, request):
        stats = Statistics.objects.all()
        serializer = StatisticsSerializer(stats, many=True)
        return Response(serializer.data)
