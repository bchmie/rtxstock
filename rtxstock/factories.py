import datetime
import random
from decimal import Decimal

import factory
from django.utils import timezone
from factory import SubFactory
from factory.django import DjangoModelFactory
from faker import Faker
from faker.providers import BaseProvider

from .models import Manufacturer, Offer, Product, Shop, Statistics

fake = Faker()


class ModelProvider(BaseProvider):
    prefixes = ("GV", "TUF", "ROG", "NED", "ZT")

    def model(self):
        code = fake.swift()
        return "-".join([random.choice(self.prefixes), "RTX3080", code, "10GB"])


factory.Faker.add_provider(ModelProvider)


class ManufacturerFactory(DjangoModelFactory):
    name = factory.Faker("company")

    class Meta:
        model = Manufacturer


class ProductFactory(DjangoModelFactory):
    model = factory.Faker("model")
    name = "RTX 3080 10GB"
    trade_item_number = factory.Faker("ean")
    manufacturer = SubFactory(ManufacturerFactory)
    msrp = Decimal(random.randint(3000, 4000))
    is_available = False
    last_seen = factory.LazyFunction(timezone.now)
    last_available = None

    class Meta:
        model = Product


class ShopFactory(DjangoModelFactory):
    name = factory.Faker("company")

    class Meta:
        model = Shop


class OfferFactory(DjangoModelFactory):
    product = SubFactory(ProductFactory)
    shop = SubFactory(ShopFactory)
    price = Decimal(random.randint(3000, 4000))
    is_available = bool(random.randint(0, 1))
    timestamp = factory.LazyFunction(timezone.now)
    link = factory.Faker("url")

    class Meta:
        model = Offer


class StatisticsFactory(DjangoModelFactory):
    date = factory.Sequence(lambda n: timezone.now() - datetime.timedelta(days=n))
    cards_available = random.randint(0, 5)
    mean_price = Decimal(random.randint(3000, 4000))
    median_price = Decimal(random.randint(3000, 4000))

    @classmethod
    def _setup_next_sequence(cls):
        try:
            earliest = datetime.date.today() - Statistics.objects.earliest("date")
            return earliest.days
        except Statistics.DoesNotExist:
            return 1

    class Meta:
        model = Statistics
