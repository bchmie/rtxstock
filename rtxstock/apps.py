from django.apps import AppConfig


class RtxstockConfig(AppConfig):
    name = "rtxstock"
