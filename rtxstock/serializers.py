from rest_framework import serializers

from rtxstock.models import Manufacturer, Offer, Product, Shop, Statistics


class ManufacturerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Manufacturer
        fields = ("id", "name")


class ShopSerializer(serializers.ModelSerializer):
    class Meta:
        model = Shop
        fields = ("id", "name")


class OfferSerializer(serializers.ModelSerializer):
    shop = ShopSerializer()

    class Meta:
        model = Offer
        fields = ("id", "product", "shop", "price", "is_available", "timestamp", "link")


class ProductSerializer(serializers.ModelSerializer):
    offers = OfferSerializer(many=True)
    manufacturer = ManufacturerSerializer()
    price = serializers.DecimalField(max_digits=7, decimal_places=2, required=False)

    class Meta:
        model = Product
        fields = (
            "model",
            "name",
            "trade_item_number",
            "manufacturer",
            "msrp",
            "is_available",
            "last_seen",
            "last_available",
            "offers",
            "price",
        )


class StatisticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Statistics
        fields = ("date", "cards_available", "mean_price", "median_price")
