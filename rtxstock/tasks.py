import datetime

from celery import shared_task
from django.core.management import call_command
from django.utils import timezone

from rtxstock.models import Statistics


@shared_task
def scrape():
    call_command("scrape")


@shared_task
def calculate_statistics(day=None):
    if day is None:
        day = timezone.now().date() - datetime.timedelta(days=1)
    Statistics.objects.create_for_date(day)
