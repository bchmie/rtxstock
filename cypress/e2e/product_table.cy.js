describe("Product table", () => {
  beforeEach(() => {
    cy.intercept("GET", "/api/v1/products/", { fixture: "products.json" });
  });

  it("Displays table", () => {
    cy.visit("");
    cy.get("[data-cy=product-table").should("exist");
    cy.get("[data-cy=product-table-header").should("contain.text", "Model");
    cy.get("[data-cy=product-table-header").should(
      "contain.text",
      "Manufacturer"
    );
    cy.get("[data-cy=product-table-header").should(
      "contain.text",
      "Availability"
    );
    cy.get("[data-cy=product-table-header").should("contain.text", "Price");
    cy.get("[data-cy=product-table-header").should("contain.text", "Last seen");
    cy.get("[data-cy=product-table-header").should("contain.text", "Offers");
  });

  it("Displays products", () => {
    cy.fixture("products.json").then((productsJson) => {
      for (const product of productsJson) {
        cy.get(`[data-cy=product-row-${product.model}`)
          .should("exist")
          .and("contain.text", product.name);
      }
    });
  });

  it("Sorts by name", () => {
    cy.visit("");
    cy.get("[data-cy=sort-name]").click();
    cy.get("[data-cy^=product-row-]").first().should("contain.text", "Bronze");

    cy.get("[data-cy=sort-name]").click();
    cy.get("[data-cy^=product-row-]").first().should("contain.text", "Silver");
  });

  it("Sorts by manufacturer", () => {
    cy.visit("");
    cy.get("[data-cy=sort-manufacturer]").trigger("click");
    cy.get("[data-cy^=product-row-]")
      .first()
      .should("contain.text", "Manufacturer1");

    cy.get("[data-cy=sort-manufacturer]").click();
    cy.get("[data-cy^=product-row-]")
      .first()
      .should("contain.text", "Manufacturer2");
  });

  it("Has filters dropdown", () => {
    cy.visit("");

    // filters should initially be hidden
    cy.get("[data-cy=filters-name").should("not.be.visible");

    cy.get("[data-cy=filters").click();
    cy.get("[data-cy=filters-name").should("be.visible");

    cy.get("[data-cy=filters").click();
    cy.get("[data-cy=filters-name").should("not.be.visible");
  });

  it("Filters by name", () => {
    cy.visit("");

    cy.get("[data-cy=filters").click();
    cy.get("[data-cy=filters-name").type("bronze");
    cy.get("[data-cy^=product-row-]").should("have.length", 1);
  });

  it("Filters by Manufacturer", () => {
    cy.visit("");

    cy.get("[data-cy=filters").click();
    cy.get("[data-cy=filters-manufacturer").type("Manufacturer1");
    cy.get("[data-cy^=product-row-]").should("have.length", 1);
  });

  it("Filters by availability", () => {
    cy.visit("");

    cy.get("[data-cy=filters").click();
    cy.get("[data-cy=filters-availability").click();
    cy.get("[data-cy^=product-row-]").should("have.length", 0);
  });

  it("Filters by price range", () => {
    cy.visit("");

    cy.get("[data-cy=filters").click();
    cy.get("[data-cy=filters-price")
      .trigger("mousedown", { which: 1 }, { force: true })
      .trigger("mousemove", 800, 0, { force: true })
      .trigger("mouseup");
    cy.get("[data-cy^=product-row-]").should("have.length", 0);
  });

  it("Clears the filters", () => {
    cy.visit("");

    cy.get("[data-cy=filters").click();

    // populate the filters
    cy.get("[data-cy=filters-name").type("bronze");
    cy.get("[data-cy=filters-manufacturer").type("Manufacturer1");
    cy.get("[data-cy=filters-availability").click();

    cy.get("[data-cy^=product-row-]").should("have.length", 0);

    cy.get("[data-cy=filters-reset").click();

    cy.get("[data-cy^=product-row-]").should("have.length", 2);
    cy.get("[data-cy=filters-name").should("have.value", "");
    cy.get("[data-cy=filters-manufacturer").should("have.value", "");
    cy.get("[data-cy=filters-availability").should("not.be.checked");
  });
});
