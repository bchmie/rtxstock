import os

from celery import Celery
from celery.schedules import crontab
from django.conf import settings

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")

app = Celery("rtxstock")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()


@app.on_after_configure.connect
def setup_periodic_tasks(sender, **kwargs):
    if settings.SCRAPING_FREQUENCY != 0:
        sender.add_periodic_task(
            settings.SCRAPING_FREQUENCY,
            sender.signature("rtxstock.tasks.scrape"),
            name="Scrape RTX stock data",
        )

    if settings.CALCULATE_STATS:
        sender.add_periodic_task(
            crontab(minute=0, hour=2),
            sender.signature("rtxstock.tasks.calculate_statistics"),
        )

    if settings.SEND_NOTIFICATIONS:
        sender.add_periodic_task(
            crontab(minute="*/10"),
            sender.signature("notifications.tasks.send_notifications"),
        )
