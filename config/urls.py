"""config URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from notifications.urls import urlpatterns as notifications_patterns
from rtxstock.views import (
    ManufacturerDetailApi,
    ManufacturerListApi,
    OfferDetailApi,
    OfferListApi,
    ProductDetailApi,
    ProductDetailOffersApi,
    ProductListApi,
    ProductPriceHistory,
    ShopDetailApi,
    ShopListApi,
    StatisticsListApi,
)

rtxstock_patterns = [
    path("manufacturers/", ManufacturerListApi.as_view(), name="manufacturers-list"),
    path(
        "manufacturers/<int:pk>/",
        ManufacturerDetailApi.as_view(),
        name="manufacturers-detail",
    ),
    path("products/", ProductListApi.as_view(), name="products-list"),
    path("products/<model>/", ProductDetailApi.as_view(), name="products-detail"),
    path(
        "products/<model>/offers/",
        ProductDetailOffersApi.as_view(),
        name="products-detail-offers",
    ),
    path(
        "products/<model>/price-history/<int:days>/",
        ProductPriceHistory.as_view(),
        name="products-price-history",
    ),
    path("shops/", ShopListApi.as_view(), name="shops-list"),
    path("shops/<int:pk>/", ShopDetailApi.as_view(), name="shops-detail"),
    path("offers/", OfferListApi.as_view(), name="offers-list"),
    path("offers/<int:pk>/", OfferDetailApi.as_view(), name="offers-detail"),
    path("statistics/", StatisticsListApi.as_view(), name="statistics-list"),
]

urlpatterns = [
    path("api/v1/", include((rtxstock_patterns, "rtxstock"))),
    path("api/v1/", include((notifications_patterns, "notifications"))),
    path("api/v1/token/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("api/v1/token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
    path("admin/", admin.site.urls),
]
if settings.DEBUG:
    urlpatterns += [
        path("__debug__/", include("debug_toolbar.urls")),
    ]
